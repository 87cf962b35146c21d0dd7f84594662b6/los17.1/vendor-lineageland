# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ALARM_PATH := vendor/lineageland/prebuilt/common/media/sounds/alarms
NOTIFICATION_PATH := vendor/lineageland/prebuilt/common/media/sounds/notifications
RINGTONE_PATH := vendor/lineageland/prebuilt/common/media/sounds/ringtones

# Alarms
PRODUCT_COPY_FILES += \
    $(ALARM_PATH)/Timer.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Timer.ogg \
    $(ALARM_PATH)/Sway.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Sway.ogg \
    $(ALARM_PATH)/Icicles.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Icicles.ogg \
    $(ALARM_PATH)/Gallop.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Gallop.ogg \
    $(ALARM_PATH)/Krypton.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Krypton.ogg \
    $(ALARM_PATH)/Argon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Argon.ogg \
    $(ALARM_PATH)/Flow.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Flow.ogg \
    $(ALARM_PATH)/Neon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Neon.ogg \
    $(ALARM_PATH)/Sunshower.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Sunshower.ogg \
    $(ALARM_PATH)/Gentle_breeze.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Gentle_breeze.ogg \
    $(ALARM_PATH)/Drip.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Drip.ogg \
    $(ALARM_PATH)/Carbon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Carbon.ogg \
    $(ALARM_PATH)/Loose_change.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Loose_change.ogg \
    $(ALARM_PATH)/Spokes.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Spokes.ogg \
    $(ALARM_PATH)/A_real_hoot.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/A_real_hoot.ogg \
    $(ALARM_PATH)/Orbit.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Orbit.ogg \
    $(ALARM_PATH)/Wag.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Wag.ogg \
    $(ALARM_PATH)/Jump_start.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Jump_start.ogg \
    $(ALARM_PATH)/Bright_morning.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Bright_morning.ogg \
    $(ALARM_PATH)/Helium.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Helium.ogg \
    $(ALARM_PATH)/Awaken.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Awaken.ogg \
    $(ALARM_PATH)/Bounce.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Bounce.ogg \
    $(ALARM_PATH)/Rolling_fog.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Rolling_fog.ogg \
    $(ALARM_PATH)/Full_of_wonder.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Full_of_wonder.ogg \
    $(ALARM_PATH)/Cuckoo_clock.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Cuckoo_clock.ogg \
    $(ALARM_PATH)/Early_twilight.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Early_twilight.ogg \
    $(ALARM_PATH)/Rise.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Rise.ogg \
    $(ALARM_PATH)/Nudge.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/Nudge.ogg


# Notifications
PRODUCT_COPY_FILES += \
    $(NOTIFICATION_PATH)/Ceres.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Ceres.ogg \
    $(NOTIFICATION_PATH)/Tweeter.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Tweeter.ogg \
    $(NOTIFICATION_PATH)/Beginning.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Beginning.ogg \
    $(NOTIFICATION_PATH)/Ariel.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Ariel.ogg \
    $(NOTIFICATION_PATH)/Elara.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Elara.ogg \
    $(NOTIFICATION_PATH)/Hey.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Hey.ogg \
    $(NOTIFICATION_PATH)/Twinkle.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Twinkle.ogg \
    $(NOTIFICATION_PATH)/Mallet.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Mallet.ogg \
    $(NOTIFICATION_PATH)/Pipes.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Pipes.ogg \
    $(NOTIFICATION_PATH)/Trill.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Trill.ogg \
    $(NOTIFICATION_PATH)/Ping.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Ping.ogg \
    $(NOTIFICATION_PATH)/Salacia.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Salacia.ogg \
    $(NOTIFICATION_PATH)/Birdsong.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Birdsong.ogg \
    $(NOTIFICATION_PATH)/Gentle_gong.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Gentle_gong.ogg \
    $(NOTIFICATION_PATH)/Duet.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Duet.ogg \
    $(NOTIFICATION_PATH)/Popcorn.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Popcorn.ogg \
    $(NOTIFICATION_PATH)/Strum.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Strum.ogg \
    $(NOTIFICATION_PATH)/Coconuts.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Coconuts.ogg \
    $(NOTIFICATION_PATH)/Carme.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Carme.ogg \
    $(NOTIFICATION_PATH)/Titan.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Titan.ogg \
    $(NOTIFICATION_PATH)/Shopkeeper.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Shopkeeper.ogg \
    $(NOTIFICATION_PATH)/Tethys.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Tethys.ogg \
    $(NOTIFICATION_PATH)/Tuneup.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Tuneup.ogg \
    $(NOTIFICATION_PATH)/Clink.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Clink.ogg \
    $(NOTIFICATION_PATH)/Note.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Note.ogg \
    $(NOTIFICATION_PATH)/Io.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Io.ogg \
    $(NOTIFICATION_PATH)/Chime.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Chime.ogg \
    $(NOTIFICATION_PATH)/Rhea.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Rhea.ogg \
    $(NOTIFICATION_PATH)/Iapetus.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Iapetus.ogg \
    $(NOTIFICATION_PATH)/End_note.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/End_note.ogg \
    $(NOTIFICATION_PATH)/Flick.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Flick.ogg \
    $(NOTIFICATION_PATH)/Sticks_and_stones.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Sticks_and_stones.ogg \
    $(NOTIFICATION_PATH)/Orders_up.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Orders_up.ogg \
    $(NOTIFICATION_PATH)/Europa.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/Europa.ogg


# Ringtones
PRODUCT_COPY_FILES += \
    $(RINGTONE_PATH)/Lollipop.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Lollipop.ogg \
    $(RINGTONE_PATH)/Phobos.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Phobos.ogg \
    $(RINGTONE_PATH)/Callisto.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Callisto.ogg \
    $(RINGTONE_PATH)/Luna.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Luna.ogg \
    $(RINGTONE_PATH)/Hotline.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Hotline.ogg \
    $(RINGTONE_PATH)/Leaps_and_bounds.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Leaps_and_bounds.ogg \
    $(RINGTONE_PATH)/Sedna.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Sedna.ogg \
    $(RINGTONE_PATH)/Copycat.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Copycat.ogg \
    $(RINGTONE_PATH)/Mash_up.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Mash_up.ogg \
    $(RINGTONE_PATH)/Triton.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Triton.ogg \
    $(RINGTONE_PATH)/Rrrring.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Rrrring.ogg \
    $(RINGTONE_PATH)/Spaceship.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Spaceship.ogg \
    $(RINGTONE_PATH)/Titania.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Titania.ogg \
    $(RINGTONE_PATH)/Lost_and_found.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Lost_and_found.ogg \
    $(RINGTONE_PATH)/Summer_night.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Summer_night.ogg \
    $(RINGTONE_PATH)/Schools_out.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Schools_out.ogg \
    $(RINGTONE_PATH)/Monkey_around.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Monkey_around.ogg \
    $(RINGTONE_PATH)/Zen.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Zen.ogg \
    $(RINGTONE_PATH)/Oberon.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Oberon.ogg \
    $(RINGTONE_PATH)/The_big_adventure.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/The_big_adventure.ogg \
    $(RINGTONE_PATH)/Dance_party.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Dance_party.ogg \
    $(RINGTONE_PATH)/Atria.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Atria.ogg \
    $(RINGTONE_PATH)/Zen_too.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Zen_too.ogg \
    $(RINGTONE_PATH)/Umbriel.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Umbriel.ogg \
    $(RINGTONE_PATH)/Beats.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Beats.ogg \
    $(RINGTONE_PATH)/Flutterby.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Flutterby.ogg \
    $(RINGTONE_PATH)/Ganymede.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Ganymede.ogg \
    $(RINGTONE_PATH)/Hey_hey.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Hey_hey.ogg \
    $(RINGTONE_PATH)/Crackle.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Crackle.ogg \
    $(RINGTONE_PATH)/Early_bird.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Early_bird.ogg \
    $(RINGTONE_PATH)/Pyxis.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Pyxis.ogg \
    $(RINGTONE_PATH)/Dione.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Dione.ogg \
    $(RINGTONE_PATH)/Romance.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Romance.ogg \
    $(RINGTONE_PATH)/Shooting_star.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/Shooting_star.ogg


